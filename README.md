## About MAQEBot

MAQEBot is a command line application based on Symfony console component. It accepts the command R,L WN to rotate right, left and walk 
N steps.

## KEY FEATURES
Accepts command line instructions to move the Bot.

## SYSTEM REQUIREMENTS
- PHP >= 7.1.3
- PHPUNIT 8.0

## INSTALLATION / SETUP

```
1. Clone the repository: 
git clone https://bitbucket.org/sirish_shrestha/maqebot.git

2. Go to project root folder and run:
composer install 

3. Console command to instruct the MAQEBot:
php index.php maqebot:move RW15RW1
Result: Outputs current position and direction of bot.
Example: X: 15 Y: -1 Direction: South

4. Run Unit Tests:
./vendor/bin/phpunit ./tests/
```