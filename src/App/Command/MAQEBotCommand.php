<?php
/**
 * PHP Version 7
 *
 * @category PHP
 * @package  Command
 * @author   Sirish Shrestha <sirish_shr@yahoo.com>
 * @license  http://www.opensource.org/licenses/mit-license.html  MIT License
 * @link     https://bitbucket.org/sirish_shrestha/maqebot.git
 */
namespace App\Command;

use App\Core\MAQEBot;
use App\Core\Position;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class MAQEBotCommand
 *
 * @category PHP
 * @package  Command
 * @author   Sirish Shrestha <sirish_shr@yahoo.com>
 * @license  http://www.opensource.org/licenses/mit-license.html  MIT License
 * @link     https://bitbucket.org/sirish_shrestha/maqebot.git
 */
class MAQEBotCommand extends Command
{
    /**
     * Set the configuration for the command
     *
     * @return void
     */
    protected function configure(): void
    {
        $this->setName('maqebot:move')
            ->setDescription('Moves the MAQE bot.')
            ->setHelp('Use this command to move the MAQE bot.')
            ->addArgument(
                'signal',
                InputArgument::REQUIRED,
                'The signals to be provided to the MAQE bot to move in 
                            a particular direction.'
            );
    }
    /**
     * Execute the command
     *
     * @param InputInterface  $input  Commandline Input String
     * @param OutputInterface $output Commandline Output String
     *
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $signal = $input->getArgument('signal') ?: "";
        $maqeBot = new MAQEBot(new Position());
        $maqeBot->move($signal);
        $output->writeln($maqeBot->getPosition());
    }
}
