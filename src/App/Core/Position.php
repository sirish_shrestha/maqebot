<?php
/**
 * PHP Version 7
 *
 * @category PHP
 * @package  MAQEBot
 * @author   Sirish Shrestha <sirish_shr@yahoo.com>
 * @license  http://www.opensource.org/licenses/mit-license.html  MIT License
 * @link     https://bitbucket.org/sirish_shrestha/maqebot.git
 */
namespace App\Core;

/**
 *  MAQEBot Position Class
 *
 * @category PHP
 * @package  MAQEBot
 * @author   Sirish Shrestha <sirish_shr@yahoo.com>
 * @license  http://www.opensource.org/licenses/mit-license.html  MIT License
 * @link     https://bitbucket.org/sirish_shrestha/maqebot.git
 */
class Position
{
    const NORTH = 0;
    const EAST = 1;
    const SOUTH = 2;
    const WEST = 3;

    protected $posX;
    protected $posY;
    protected $direction;
    protected $directionNames = ["North","East","South","West"];

    /**
     *  Initialize the starting position of the MAQEBOT
     */
    public function __construct()
    {
        $this->direction = self::NORTH;
        $this->posX = 0;
        $this->posY = 0;
    }

    /**
     * Function to turn the Bot to Right direction
     *
     * @return $this
     */
    public function turnRight(): Position
    {
        /**
         *  For Clockwise Rotation add 1 to current direction and
         *  make sure it does not reach 4 which
         *  is an invalid direction. Valid directions = 0,1,2,3
         */
        $this->direction += 1;
        if ($this->direction > self::WEST) {
            $this->direction = self::NORTH;
        }
        return $this;
    }

    /**
     * Function to turn the Bot to Left direction
     *
     * @return $this
     */
    public function turnLeft(): Position
    {
        /**
         *  For Anti-Clockwise Rotation subtract -1 to current direction
         *  and make sure it does not reach -1 which
         *  is an invalid direction. Valid directions = 0,1,2,3
         */
        $this->direction -= 1;
        if ($this->direction < self::NORTH) {
            $this->direction = self::WEST;
        }
        return $this;
    }

    /**
     * Move the Bot with N steps in its current direction
     *
     * @param int $steps Number of Steps to move
     *
     * @return $this
     */
    public function walk($steps = 1): Position
    {
        switch ($this->direction) {
            case self::NORTH:
                $this->posY += $steps;
                break;
            case self::EAST:
                $this->posX += $steps;
                break;
            case self::SOUTH:
                $this->posY -= $steps;
                break;
            case self::WEST:
                $this->posX -= $steps;
                break;
        }
        return $this;
    }

    /**
     * Returns the current position of the MAQEBot in array
     *
     * @return array
     */
    public function getPosition(): array
    {
        return [$this->posX, $this->posY, $this->directionNames[$this->direction]];
    }
}
