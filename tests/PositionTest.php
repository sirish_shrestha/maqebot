<?php
use PHPUnit\Framework\TestCase;

class PositionTest extends TestCase
{

    protected $pos;

    /**
     * Set Up the Test Case for Position Class
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->pos = new \App\Core\Position();
    }


    public function testInitialPosition()
    {
        $this->assertEquals([0,0,"North"],$this->pos->getPosition());
    }

    public function testTurnRightPosition()
    {
        $this->pos->turnRight();
        $this->assertEquals([0,0,"East"], $this->pos->getPosition());
    }

    public function testTurnLeftPosition()
    {
        $this->pos->turnLeft();
        $this->assertEquals([0,0,"West"], $this->pos->getPosition());
    }

    public function test360ClockwiseRotationPosition()
    {
        $this->pos->turnRight()
            ->turnRight()
            ->turnRight()
            ->turnRight();
        $this->assertEquals([0,0,"North"], $this->pos->getPosition());
    }

    public function test360AntiClockwiseRotation()
    {
        $this->pos->turnLeft()
            ->turnLeft()
            ->turnLeft()
            ->turnLeft();
        $this->assertEquals([0,0,"North"], $this->pos->getPosition());
    }


    public function testWalkOneStep()
    {
        $this->pos->walk(1);
        $this->assertEquals([0,1,"North"], $this->pos->getPosition());
    }
}