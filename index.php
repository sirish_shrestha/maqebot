#!/usr/bin/env php
<?php
/**
 * * PHP Version 7
 *
 * @category PHP
 * @package  MAQEBot
 * @author   Sirish Shrestha <sirish_shr@yahoo.com>
 * @license  http://www.opensource.org/licenses/mit-license.html  MIT License
 * @link     https://bitbucket.org/sirish_shrestha/maqebot.git
 */

require __DIR__ . '/vendor/autoload.php';

use App\Command\MAQEBotCommand;
use Symfony\Component\Console\Application;

$app = new Application("MAQE Bot", 1.0);
$app->add(new MAQEBotCommand());
try{
    $app->run();
} catch (\Exception $e){
    echo $e->errorMessage();
}