<?php

use PHPUnit\Framework\TestCase;

class MAQEBotTest extends TestCase
{

    protected $bot;

    /**
     * Set Up the Test Case for MAQEBot Class
     */
    public function setUp(): void
    {
        parent::setUp();
        /**
         * MOCK the Position dependency
         */
        $mockPosition = $this->getMockBuilder('App\Core\Position')
            ->setMethods(array('update'))
            ->getMock();
        $this->bot = new \App\Core\MAQEBot($mockPosition);
    }


    public function testMoveOneStep()
    {
        $this->bot->move('W1');
        $this->assertEquals("X: 0 Y: 1 Direction: North", $this->bot->getPosition());
    }

    public function testTurnRight()
    {
        $this->bot->move('R');
        $this->assertEquals("X: 0 Y: 0 Direction: East", $this->bot->getPosition());
    }

    public function testTurnLeft()
    {
        $this->bot->move('L');
        $this->assertEquals("X: 0 Y: 0 Direction: West", $this->bot->getPosition());
    }

    public function test360ClockwiseRotation()
    {
        $this->bot->move('RRRR');
        $this->assertEquals("X: 0 Y: 0 Direction: North", $this->bot->getPosition());
    }

    public function test360AntiClockwiseRotation()
    {
        $this->bot->move('LLLL');
        $this->assertEquals("X: 0 Y: 0 Direction: North", $this->bot->getPosition());
    }


    public function testInvalidSignals()
    {
        $command = 'W1RL9R10';
        $this->expectException(Exception::class);
        $this->bot->move($command);
    }

    public function testSampleData()
    {
        $command = 'W5RW5RW2RW1R';
        $this->bot->move($command);
        $this->assertEquals("X: 4 Y: 3 Direction: North", $this->bot->getPosition());
    }
}