<?php
/**
 * PHP Version 7
 *
 * @category PHP
 * @package  MAQEBot
 * @author   Sirish Shrestha <sirish_shr@yahoo.com>
 * @license  http://www.opensource.org/licenses/mit-license.html  MIT License
 * @link     https://bitbucket.org/sirish_shrestha/maqebot.git
 */
namespace App\Core;

/**
 * Class Main MAQEBot
 *
 * @category PHP
 * @package  MAQEBot
 * @author   Sirish Shrestha <sirish_shr@yahoo.com>
 * @license  http://www.opensource.org/licenses/mit-license.html  MIT License
 * @link     https://bitbucket.org/sirish_shrestha/maqebot.git
 */
class MAQEBot
{
    /**
     * @var Position Position Object
     */
    protected $position;

    /**
     * MAQEBot constructor.
     *
     * @param Position $pos Inject position object
     *
     * @return void
     */
    public function __construct(Position $pos)
    {
        $this->position = $pos;
    }

    /**
     * Function to parse the whole signal and move the MAQEBot step by step.
     *
     * @param string $signal The raw input signal coming from the command line
     *
     * @return void
     */
    public function move($signal): void
    {
        $isValid = $this->isValidSignal($signal);
        if (!$isValid) {
            throw new \InvalidArgumentException("Invalid Signal '$signal'");
        }
        $commands = $this->getSingleMoveCommand($signal);
        foreach ($commands as $command) {
            $commandInitials = mb_substr($command[0], 0, 1, 'utf-8');
            if ($commandInitials == "R") {
                $this->position->turnRight();
            } elseif ($commandInitials == "L") {
                $this->position->turnLeft();
            } elseif ($commandInitials == "W") {
                $numberOfSteps = $command[1] ?? "Invalid Signal W without N steps in '{$signal}'";
                if (!is_numeric($numberOfSteps)) {
                    throw new \InvalidArgumentException($numberOfSteps);
                }
                $this->position->walk($numberOfSteps);
            } elseif ($commandInitials !== "") {
                throw new \InvalidArgumentException(sprintf("Invalid Signal '%s' in '%s'", $command[0], $signal));
            }
        }
    }

    /**
     * Function to return each single bot command in an array
     * Example of the array format of returning value
     * ([0] => Array ([0] => W8
     *                [1] => 8)
     *  [1] => Array ([0] => R))
     *
     * @param string $signal The validated input signal coming from the command line
     *
     * @return array (See above)
     */
    public function getSingleMoveCommand($signal): array
    {
        preg_match_all('/R|L|W(\d+)|.+/i', $signal, $commands, PREG_SET_ORDER);
        return $commands ?: [];
    }

    /**
     *  Validates the Signal given to the Bot. Allowed Characters: R L W and digits
     *
     * @param string $signal The raw input signal coming from the command line
     *
     * @return bool
     */
    public function isValidSignal($signal): bool
    {
        if (!preg_match("/^[RLW(\d)]+$/i", $signal)) {
            return false;
        }
        return true;
    }

    /**
     * Get the current position of MAQEBot in string
     *
     * @return string
     */
    public function getPosition(): string
    {
        $position = $this->position->getPosition();
        return "X: {$position[0]} Y: {$position[1]} Direction: {$position[2]}";
    }
}
